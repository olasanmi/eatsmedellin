export function getFoodTypeLists (state) {
	return state.typeFoodList
}

export function getFilterFood (state) {
	return state.foodFilter
}
