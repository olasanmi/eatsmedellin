import { getFoodType, restaurantByFoodType, restaurantByArea, restaurantByPrice, foodFilter } from './../../api/apiCall'

export function getTypeFood ({ commit }) {
	return new Promise ((resolve, reject) => {
		return getFoodType((response) => {
			commit('setFoodType', response.data)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function filterRestaurant({ commit }, id) {
	return new Promise ((resolve, reject) => {
		return restaurantByFoodType(id, (response) => {
			// commit('setFoodType', response.data)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function filterRestaurantArea({ commit }, id) {
	return new Promise ((resolve, reject) => {
		return restaurantByArea(id, (response) => {
			// commit('setFoodType', response.data)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function filterRestaurantPrice({ commit }, id) {
	return new Promise ((resolve, reject) => {
		return restaurantByPrice(id, (response) => {
			// commit('setFoodType', response.data)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function filterFoodById({ commit }, id) {
	return new Promise ((resolve, reject) => {
		return foodFilter(id, (response) => {
			commit('setFoodFilter', response.data)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}
