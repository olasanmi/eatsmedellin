import { registerUser, loginUser, getRestaurant, recoveryChangePassword, changePasssWord } from './../../api/apiCall'

export function register ({ commit }, user) {
	return new Promise((resolve, reject) => {
		return registerUser(user, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function login ({ commit }, login) {
	return new Promise ((resolve, reject) => {
		localStorage.removeItem('userId')
  		localStorage.removeItem('user')
		return loginUser(login, (response) => {
			localStorage.setItem('user', JSON.stringify(response.data.Success))
			localStorage.setItem('userid', response.data.Success.id)
			commit('loginSuccess', response.data.Success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function logout ({ commit }) {
	return new Promise ((resolve, reject) => {
		localStorage.removeItem('userId')
		localStorage.removeItem('user')
		commit('logout')
		resolve()
	})
}

export function getRestaurants ({ commit }) {
	return new Promise ((resolve, reject) => {
		return getRestaurant((response) => {
			commit('setRestaurant', response.data)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function changePassword ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return recoveryChangePassword(data, (response) => {
			commit('setUserIdToEdit', response.data.user)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function sendNewPassWord ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return changePasssWord(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function loginGooglePluss ({ commit }, data) {
	return new Promise ((resolve, reject) => {
		localStorage.setItem('user', JSON.stringify(data.userData))
		localStorage.setItem('userid', data.userData.displayName)
		commit('loginSuccessGooglePluss', data.userData)
		resolve(data.userData)
	})
}
