import Vue from 'vue'
import Vuesax from 'vuesax'

import 'vuesax/dist/vuesax.css'

export default async () => {
  Vue.use(Vuesax)
}
