export const setFoodType = (state, data) => {
	state.typeFoodList = data.data
}

export const setFoodFilter = (state, data) => {
	state.foodFilter = data.data
}
